﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using WindowsInput.Native;
using WindowsInput;

namespace IsActiveStreamView
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string StopThread;
        string flux;
        
        Thread ThreadTemp;
        Thread ThreadTempKey;

        [System.Runtime.InteropServices.DllImport("USER32.DLL", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [System.Runtime.InteropServices.DllImport("USER32.DLL", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            if (ThreadTemp != null)
            {
                ThreadTemp.Abort();
            }

            if (ThreadTempKey != null)
            {
                ThreadTempKey.Abort();
            }

            base.OnFormClosing(e);
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            Console.WriteLine("Started !");

            flux = "0";

            //string ClientName1 = "League of Legends";
            //string ClientName2 = "League of Legends (TM) Client";

            comboBox3.Text = "CTRL+MAJ+0";
            comboBox4.Text = "CTRL+MAJ+1";

            for (int i = 0; i < 10; i++)
            {
                comboBox3.Items.Add("CTRL+MAJ+" + i);
                comboBox4.Items.Add("CTRL+MAJ+" + i);
            }
        }

        public void ExecThreadSmell(string ClientName1, string ClientName2, string ChangeScene1, string ChangeScene2)
        {
            ThreadTemp = new Thread(() =>
            {
                for (int i = 0; i < 1000; i++)
                {

                    if (StopThread == "2")
                    {
                        break;
                    }

                    try
                    {

                        var c = new tools();

                        string title = c.ActiveWindowTitle();

                        BeginInvoke(new Action(() =>
                        {
                            InsertRichTextBox("Id " + i.ToString() + ": " + DateTime.Now.ToString("hh:mm:ss") + " - " + title + "\n");
                        }));

                        if (title != "")
                        {

                            if (title == ClientName1)
                            {
                                if(flux == "1")
                                {
                                    goto pass;
                                }

                                PressShortcut1(ChangeScene1.Substring(9, 1));

                                flux = "1";
                            }

                            if (title == ClientName2)
                            {

                                //MessageBox.Show("HELLo");

                                if (flux == "2")
                                {
                                    goto pass;
                                }

                                PressShortcut2(ChangeScene2.Substring(9, 1));

                                flux = "2";
                            }

                            if (title != ClientName1 & title != ClientName2)
                            {
                                flux = "0";
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Erreur : " + ex.Message);
                    }


                pass:;

                    Thread.Sleep(450);

                }
            });
            ThreadTemp.SetApartmentState(ApartmentState.STA);
            ThreadTemp.Start();

            if (StopThread == "2")
            {
                ThreadTemp.Abort();
            }

        }

        public void InsertRichTextBox(string input)
        {
            BeginInvoke(new Action(() =>
            {
                richTextBox1.Select(richTextBox1.TextLength, 0);
                richTextBox1.ScrollToCaret();
                richTextBox1.AppendText(input);

            }));
        }

        public class tools
        {

            [DllImport("user32.dll")]
            static extern IntPtr GetForegroundWindow();
            [DllImport("user32.dll")]
            static extern int GetWindowText(IntPtr hwnd, StringBuilder ss, int count);
            public string ActiveWindowTitle()
            {
                //Create the variable
                const int nChar = 256;
                StringBuilder ss = new StringBuilder(nChar);

                //Run GetForeGroundWindows and get active window informations
                //assign them into handle pointer variable
                IntPtr handle = IntPtr.Zero;
                handle = GetForegroundWindow();

                if (GetWindowText(handle, ss, nChar) > 0) return ss.ToString();
                else return "";
            }


        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string ClientName1 = comboBox1.Text;
            string ClientName2 = comboBox2.Text;

            if (ClientName1.Replace(" ", "") == "")
            {
                MessageBox.Show("Merci de renseigner l'application N°1", "Erreur !", MessageBoxButtons.OK);
                goto endThread;
            }

            if (ClientName2.Replace(" ", "") == "")
            {
                MessageBox.Show("Merci de renseigner l'application N°2", "Erreur !", MessageBoxButtons.OK);
                goto endThread;
            }

            string ChangeScene1 = comboBox3.Text;
            string ChangeScene2 = comboBox4.Text;

            StopThread = "1";
            comboBox1.Enabled = false;
            comboBox2.Enabled = false;
            comboBox3.Enabled = false;
            comboBox4.Enabled = false;

            ExecThreadSmell(ClientName1, ClientName2, ChangeScene1, ChangeScene2);

        endThread:;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            StopThread = "2";
            comboBox1.Enabled = true;
            comboBox2.Enabled = true;
            comboBox3.Enabled = true;
            comboBox4.Enabled = true;
        }

        [DllImport("user32.dll", SetLastError = true)]
        static extern void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);

        private void button3_Click(object sender, EventArgs e)
        {
            

        }

        public void PressShortcut1(string key)
        {
            ThreadTempKey = new Thread(() =>
            {
                InputSimulator sim = new InputSimulator();

                sim.Keyboard.KeyDown(VirtualKeyCode.CONTROL);
                //sim.Keyboard.KeyDown(VirtualKeyCode.SHIFT);
                if (key == "0") { sim.Keyboard.KeyDown(VirtualKeyCode.NUMPAD0); }
                if (key == "1") { sim.Keyboard.KeyDown(VirtualKeyCode.NUMPAD1); }
                if (key == "2") { sim.Keyboard.KeyDown(VirtualKeyCode.NUMPAD2); }
                if (key == "3") { sim.Keyboard.KeyDown(VirtualKeyCode.NUMPAD3); }
                if (key == "4") { sim.Keyboard.KeyDown(VirtualKeyCode.NUMPAD4); }
                if (key == "5") { sim.Keyboard.KeyDown(VirtualKeyCode.NUMPAD5); }
                if (key == "6") { sim.Keyboard.KeyDown(VirtualKeyCode.NUMPAD6); }
                if (key == "7") { sim.Keyboard.KeyDown(VirtualKeyCode.NUMPAD7); }
                if (key == "8") { sim.Keyboard.KeyDown(VirtualKeyCode.NUMPAD8); }
                if (key == "9") { sim.Keyboard.KeyDown(VirtualKeyCode.NUMPAD9); }

                Thread.Sleep(50);

                sim.Keyboard.KeyUp(VirtualKeyCode.CONTROL);
                //sim.Keyboard.KeyUp(VirtualKeyCode.SHIFT);
                if (key == "0") { sim.Keyboard.KeyUp(VirtualKeyCode.NUMPAD0); }
                if (key == "1") { sim.Keyboard.KeyUp(VirtualKeyCode.NUMPAD1); }
                if (key == "2") { sim.Keyboard.KeyUp(VirtualKeyCode.NUMPAD2); }
                if (key == "3") { sim.Keyboard.KeyUp(VirtualKeyCode.NUMPAD3); }
                if (key == "4") { sim.Keyboard.KeyUp(VirtualKeyCode.NUMPAD4); }
                if (key == "5") { sim.Keyboard.KeyUp(VirtualKeyCode.NUMPAD5); }
                if (key == "6") { sim.Keyboard.KeyUp(VirtualKeyCode.NUMPAD6); }
                if (key == "7") { sim.Keyboard.KeyUp(VirtualKeyCode.NUMPAD7); }
                if (key == "8") { sim.Keyboard.KeyUp(VirtualKeyCode.NUMPAD8); }
                if (key == "9") { sim.Keyboard.KeyUp(VirtualKeyCode.NUMPAD9); }

            });
            ThreadTempKey.SetApartmentState(ApartmentState.STA);
            ThreadTempKey.Start();
        }

        public void PressShortcut2(string key)
        {
            ThreadTempKey = new Thread(() =>
            {
                InputSimulator sim = new InputSimulator();

                sim.Keyboard.KeyDown(VirtualKeyCode.CONTROL);
                //sim.Keyboard.KeyDown(VirtualKeyCode.SHIFT);
                if (key == "0") { sim.Keyboard.KeyDown(VirtualKeyCode.NUMPAD0); }
                if (key == "1") { sim.Keyboard.KeyDown(VirtualKeyCode.NUMPAD1); }
                if (key == "2") { sim.Keyboard.KeyDown(VirtualKeyCode.NUMPAD2); }
                if (key == "3") { sim.Keyboard.KeyDown(VirtualKeyCode.NUMPAD3); }
                if (key == "4") { sim.Keyboard.KeyDown(VirtualKeyCode.NUMPAD4); }
                if (key == "5") { sim.Keyboard.KeyDown(VirtualKeyCode.NUMPAD5); }
                if (key == "6") { sim.Keyboard.KeyDown(VirtualKeyCode.NUMPAD6); }
                if (key == "7") { sim.Keyboard.KeyDown(VirtualKeyCode.NUMPAD7); }
                if (key == "8") { sim.Keyboard.KeyDown(VirtualKeyCode.NUMPAD8); }
                if (key == "9") { sim.Keyboard.KeyDown(VirtualKeyCode.NUMPAD9); }

                Thread.Sleep(50);

                sim.Keyboard.KeyUp(VirtualKeyCode.CONTROL);
                //sim.Keyboard.KeyUp(VirtualKeyCode.SHIFT);
                if (key == "0") { sim.Keyboard.KeyUp(VirtualKeyCode.NUMPAD0); }
                if (key == "1") { sim.Keyboard.KeyUp(VirtualKeyCode.NUMPAD1); }
                if (key == "2") { sim.Keyboard.KeyUp(VirtualKeyCode.NUMPAD2); }
                if (key == "3") { sim.Keyboard.KeyUp(VirtualKeyCode.NUMPAD3); }
                if (key == "4") { sim.Keyboard.KeyUp(VirtualKeyCode.NUMPAD4); }
                if (key == "5") { sim.Keyboard.KeyUp(VirtualKeyCode.NUMPAD5); }
                if (key == "6") { sim.Keyboard.KeyUp(VirtualKeyCode.NUMPAD6); }
                if (key == "7") { sim.Keyboard.KeyUp(VirtualKeyCode.NUMPAD7); }
                if (key == "8") { sim.Keyboard.KeyUp(VirtualKeyCode.NUMPAD8); }
                if (key == "9") { sim.Keyboard.KeyUp(VirtualKeyCode.NUMPAD9); }

            });
            ThreadTempKey.SetApartmentState(ApartmentState.STA);
            ThreadTempKey.Start();
        }
    }
}
